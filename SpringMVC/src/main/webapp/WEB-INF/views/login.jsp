<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<head>

	<spring:url value="/resources/css/style.css" var="mainCss" />
	<spring:url value="/resources/js/popup.js" var="popuP" />
	
	<link href="${mainCss}" rel="stylesheet" />
    <script src="${popuP}"></script>

<title>Ticketing Application</title>
</head>
<body background="https://d2gg9evh47fn9z.cloudfront.net/800px_COLOURBOX19168963.jpg">
    <p><font color="red" style="position:relative; left:800px; top:250px;">${errorMessage}</font></p>
    
    <form action="/login" method="POST" name="li">
        
        <div class="input-group mb-3" style="position:relative; left:500px; top:350px; width: 691px;">
 		 	<div class="input-group-prepend">
    			<span class="input-group-text" id="basic-addon1">Username</span>
  			</div>
 		 <input name="name" type="text" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
		</div>
		
		<div class="input-group mb-3" style="position:relative; left:500px; top:350px; width: 691px;">
 		 	<div class="input-group-prepend">
    			<span class="input-group-text" id="basic-addon1">Password</span>
  			</div>
 		 <input name="password" type="password" class="form-control" placeholder="Username" aria-label="Username" aria-describedby="basic-addon1">
		</div>
		
		<label  class="radio-inline" style="position:relative;left:750px; top:350px">
   		<input type="radio" name="radioName" id="customer"  value="customer" checked="checked"/>Customer
 		</label>
 		<label  class="radio-inline" style="position:relative;left:760px; top:350px">
   		<input  type="radio" name="radioName"  id="staff" value="admin"/>Administrator
 		</label>
		
        <button name="buton" value="confirm" type="submit" class="btn btn-warning" style="position:relative; left:500px; top:400px;">Confirm</button>
        <button class="btn btn-warning" type="button" onclick="openForm()" style="position:relative; left:500px; top:400px;">Signup</button>
    </form>
    
 
 
<div class="form-popup" id="myForm">
  <form action="/login" method="POST" name="su" class="form-container">
    <h1>Signup</h1>

    <label for="username"><b>Username</b></label>
    <input type="text" name="username" placeholder="Enter username" required>

    <label for="psw"><b>Password</b></label>
    <input type="password" name="upass" placeholder="Enter Password" required>
    
     <label for="psw"><b>Surname</b></label>
    <input type="text" name="prenume" placeholder="Enter Surname" required>
    
    <label for="name"><b>Name</b></label>
    <input type="text" name="uName" placeholder="Enter Name" required>
    
     <label for="adress"><b>Adress</b></label>
    <input type="text" placeholder="Enter adress" name="adress" required>

    <button type="submit" class="btn" name="buton" value="confirm signup">Confirm signup</button>
    <button type="button" class="btn cancel" onclick="closeForm()">Close</button>
  </form>
</div>
    
</body>
</html>