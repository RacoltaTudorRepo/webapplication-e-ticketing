<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<head>
<meta charset="ISO-8859-1">
<title>Admin homepage</title>
</head>
<body style="background-color: orange;">
		
		<span class="badge badge-pill badge-warning" style="position:relative;top:600px;left:700px;">Choose what you want to do:</span>
		
 		


<table class="table table-borderless table-dark" style="width:20%; border-collapse: collapse;position:relative;top:600px">
  <thead>
    <tr>
      <th scope="col">Admin Details</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <th scope="row"></th>
      <td>Nume:  ${nume}</td>
    </tr>
     <tr>
      <th scope="row"></th>
      <td>Prenume:   ${prenume}</td>
    </tr>
     <tr>
      <th scope="row"></th>
      <td>Adresa:   ${adresa}</td>
    </tr>
    <tr>
      <th scope="row"></th>
      <td>Rol:   ${role}</td>
    </tr>
  </tbody>
</table>


	<table class="table table-dark" style="position:relative;top:-200px;">
	  <thead>
	    <tr>
	      <th scope="col">ID</th>
	      <th scope="col">Artist</th>
	      <th scope="col">City</th>
	      <th scope="col">Location</th>
	      <th scope="col">Date</th>
	      <th scope="col">Quantity</th>
	      <th scope="col">Price</th>				
	    </tr>
	  </thead>
	  <tbody>	  	
		  <c:forEach var="ticket" items="${ticketList}">
		  		<tr>
		        <th scope="row">${ticket.id}</th>
		        <td>${ticket.artist}</td>
		        <td>${ticket.city}</td>
		        <td>${ticket.location}</td>
		        <td>${ticket.date}</td>
		        <td>${ticket.quantity}</td>
		        <td>${ticket.price}</td>
		       </tr>
		</c:forEach>
  	   </tbody>
      </table>

	<form action="/adminHome" method="POST"> 	
			<button type="submit" name="button" value="submit" class="btn btn-danger" style="position:relative; left:1000px; top:200px;">Confirm action</button>
			<button type="submit" name="button" value="logout" class="btn btn-dark" style="position:relative; left:1400px; top:270px;">Logout</button>
			
		<label  class="radio-inline" style="position:relative;left:750px; top:4px">
   		<input type="radio" name="radioVal" id="update"  value="update" checked="checked"/><b>UPDATE</b>
 		</label>
 		<label  class="radio-inline" style="position:relative;left:760px; top:4px">
   		<input  type="radio" name="radioVal"  id="delete" value="delete"/><b>DELETE</b>
 		</label>
 		<label  class="radio-inline" style="position:relative;left:760px; top:4px">
   		<input  type="radio" name="radioVal"  id="insert" value="insert"/><b>INSERT</b>
 		</label>
			
	
	
			<div class="input-group mb-3" style="position:relative; left:500px;top:50px;width:25%">
		  <div class="input-group-prepend">
		    <label class="input-group-text" for="inputGroupSelect01">Ticket ID you wish to modify:</label>
		  </div>
		  <select name="value" class="custom-select" id="inputGroupSelect01">
		  	<c:forEach var="ticket" items="${ticketList}">    
			    <option value="${ticket.id}">${ticket.id}</option>
			</c:forEach>
		  </select>
		</div>
		
		<div class="input-group mb-3" style="position:relative; left:500px;top:100px;width:25%">
		  <div class="input-group-prepend">
		    <label class="input-group-text" for="inputGroupSelect01">What you wish to modify:</label>
		  </div>
		  <select name="by" class="custom-select" id="inputGroupSelect01">
		  		<option value="Price">Price</option>
			    <option value="City">City</option>
			    <option value="Location">Location</option>
			    <option value="Quantity">Quantity</option>
			    <option value="Date">Date</option>		    
		  </select>
		</div>
		
		<div class="input-group mb-3" style="position:relative; left:500px;top:100px;width:10%">
		  <input name="newValue" type="text" class="form-control" placeholder="New value" aria-label="New value" aria-describedby="basic-addon1">
		</div>
		
		<!-- INSERT -->
		
		
		
			<div class="input-group" style="position:relative; left:500px;top:130px;width:50%">
			  <div class="input-group-prepend">
			    <span class="input-group-text">Ticket details</span>
			  </div>
			  <input type="text" name="newID" placeholder="ID" class="form-control">
			  <input type="text" name="newArtist" placeholder="Artist" class="form-control">
			  <input type="text" name="newCity" placeholder="City" class="form-control">
			  <input type="text" name="newLocation" placeholder="Location" class="form-control">
			  <input type="text" name="newDate" placeholder="Date" class="form-control">
			  <input type="text" name="newQuantity" placeholder="Quantity" class="form-control">
			  <input type="text" name="newPrice" placeholder="Price" class="form-control">
			</div>
		
		
	
	</form>



</body>
</html>