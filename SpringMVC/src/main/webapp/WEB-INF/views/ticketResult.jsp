<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page import="model.*" %>

<!DOCTYPE html>
<html>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<head>
<meta charset="ISO-8859-1">
<title>Ticket Application</title>
</head>
<body>
	
	<span class="badge badge-pill badge-warning" style="position:relative;top:50px;left:200px;">Tickets available matching your preference:</span>
	<p class="font-weight-bold" style="color:red;position:relative;left:500px">${message}</p>
	<table class="table" style="position:relative;top:100px;">
	  <thead class="thead-dark">
	    <tr>
	      <th scope="col">ID</th>
	      <th scope="col">Artist</th>
	      <th scope="col">City</th>
	      <th scope="col">Location</th>
	      <th scope="col">Date</th>
	      <th scope="col">Quantity</th>
	      <th scope="col">Price</th>				
	    </tr>
	  </thead>
	  <tbody>	  	
		  <c:forEach var="ticket" items="${ticketList}">
		  		<tr>
		        <th scope="row">${ticket.id}</th>
		        <td>${ticket.artist}</td>
		        <td>${ticket.city}</td>
		        <td>${ticket.location}</td>
		        <td>${ticket.date}</td>
		        <td>${ticket.quantity}</td>
		        <td>${ticket.price}</td>
		       </tr>
		</c:forEach>
  	   </tbody>
      </table>
      
      
      <form action="/ticketResult" method="POST"> 	
			<button type="submit" class="btn btn-danger" style="position:relative; left:1000px; top:438px;">Buy</button>
	
	
	
	
			<div class="input-group mb-3" style="position:relative; left:500px;top:400px;width:25%">
		  <div class="input-group-prepend">
		    <label class="input-group-text" for="inputGroupSelect01">Choose ticket ID you wish to buy:</label>
		  </div>
		  <select name="value" class="custom-select" id="inputGroupSelect01">
		  	<option selected>Choose..</option>
		  	<c:forEach var="ticket" items="${ticketList}">    
			    <option value="${ticket.id}">${ticket.id}</option>
			</c:forEach>
		  </select>
		</div>
		
		<div class="input-group mb-3" style="position:relative; left:500px;top:400px;width:10%">
		  <input name="quantity" type="text" class="form-control" placeholder="Quantity" aria-label="Quantity" aria-describedby="basic-addon1">
		</div>
	
	</form>

</body>
</html>