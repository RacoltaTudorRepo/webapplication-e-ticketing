package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.User;
/**Clasa folosita pentru actiuni asupra bazei de date User*/
public class UserDAO {
	protected Connection connection;
	public void setConnection(Connection connection) {
		this.connection=connection;
	}
	/**Metoda pentru inseare user*/
	public void insertObject(User obj) throws SQLException {
		PreparedStatement insert=null;
		String insertStatementString = "INSERT INTO User (userName,nume,prenume,pasword,adresa,role)"
					+ " VALUES (?,?,?,?,?,?)";
		insert=connection.prepareStatement(insertStatementString);
		insert.setString(1, obj.getUserName());
		insert.setString(2, obj.getNume());
		insert.setString(3, obj.getPrenume());
		insert.setString(4, obj.getPassword());
		insert.setString(5, obj.getAdresa());
		insert.setString(6, obj.getRole());
		insert.executeUpdate();
		}
	/**Metoda pentru returnarea tuturor userilor*/
	public ArrayList<User> returnAllUsers() throws SQLException {
		ArrayList <User> clienti=new ArrayList<User>();
		String selectString = "SELECT * FROM USER";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			if(rs.getString(6).equals("user"))
				clienti.add(new User(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6)));
		return clienti;
	}
	/**Metoda pentru returnarea userului dupa nume*/
	public User findUserByUserName(String userName) throws SQLException {
		String selectString = "SELECT * FROM USER WHERE userName=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setString(1, userName);
		rs=statement.executeQuery();
		while(rs.next())
			return new User(rs.getString(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getString(6));
		return null;
	}
}
