package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Ticket;
/**Clasa folosita pentru actiuni asupra bazei de date Ticket*/
public class TicketDAO {
	protected Connection connection;
	public void setConnection(Connection connection) {
		this.connection=connection;
	}
	/**Metoda pentru returnarea tuturor biletelor*/
	public ArrayList<Ticket> returnAllTickets() throws SQLException {
		ArrayList<Ticket> tickets=new ArrayList<Ticket>();
		String selectString = "SELECT * FROM TICKET";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		rs=statement.executeQuery();
		while(rs.next())
			tickets.add(new Ticket(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getInt(7)));
		return tickets;
	}
	/**Metoda pentru returnarea biletelor dupa artist*/
	public ArrayList<Ticket> returnTicketsByArtist(String artist) throws SQLException {
		ArrayList<Ticket> tickets=new ArrayList<Ticket>();
		String selectString = "SELECT * FROM TICKET WHERE artist=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setString(1, artist);
		rs=statement.executeQuery();
		while(rs.next())
			tickets.add(new Ticket(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getInt(7)));
		return tickets;
	}
	/**Metoda pentru returnarea biletelor dupa oras*/
	public ArrayList<Ticket> returnTicketsByCity(String city) throws SQLException {
		ArrayList<Ticket> tickets=new ArrayList<Ticket>();
		String selectString = "SELECT * FROM TICKET WHERE city=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setString(1, city);
		rs=statement.executeQuery();
		while(rs.next())
			tickets.add(new Ticket(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getInt(7)));
		return tickets;
	}
	/**Metoda pentru returnarea biletelor dupa data*/
	public ArrayList<Ticket> returnTicketsByDate(String date) throws SQLException {
		ArrayList<Ticket> tickets=new ArrayList<Ticket>();
		String selectString = "SELECT * FROM TICKET WHERE date=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setString(1, date);
		rs=statement.executeQuery();
		while(rs.next())
			tickets.add(new Ticket(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getInt(7)));
		return tickets;
	}
	/**Metoda pentru returnarea biletelor dupa id*/
	public Ticket returnTicketByID(int id) throws SQLException{
		String selectString = "SELECT * FROM TICKET WHERE id=?";
		PreparedStatement statement=null;
		ResultSet rs=null;
		statement=connection.prepareStatement(selectString);
		statement.setInt(1, id);
		rs=statement.executeQuery();
		while(rs.next())
			return new Ticket(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(4),rs.getString(5),rs.getInt(6),rs.getInt(7));
		return null;
	}
	/**Metoda pentru actualizare cantitate bilet*/
	public void updateTicket(Ticket ticket, int q, String op) throws SQLException{
		String querry = "UPDATE ticket SET id=?, artist=?, city=?,location=?,date=?, quantity=?,price=?  WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, ticket.getId());
        statement.setString(2, ticket.getArtist());
        statement.setString(3, ticket.getCity());
        statement.setString(4, ticket.getLocation());
        statement.setString(5, ticket.getDate());
        if(op.equals("-"))
        	statement.setInt(6,ticket.getQuantity()-q);
        else statement.setInt(6,ticket.getQuantity()+q);
        statement.setInt(7, ticket.getPrice());
        statement.setInt(8, ticket.getId());
        statement.executeUpdate();
	}
	/**Metoda pentru actualizare bilet dupa criteriu ales*/
	public void updateTicketByAdmin(Ticket ticket, String newValue, String by) throws SQLException{
		String querry = "UPDATE ticket SET id=?, artist=?, city=?,location=?,date=?, quantity=?,price=?  WHERE id = ?";
        PreparedStatement statement = null;
        statement = connection.prepareStatement(querry);
        statement.setInt(1, ticket.getId());
        statement.setString(2, ticket.getArtist());
        
        if(by.equals("City")) {
        	statement.setString(3, newValue);
        }
        else statement.setString(3, ticket.getCity());
        
        if(by.equals("Location")) {
        	statement.setString(4, newValue);
        }
        else statement.setString(4, ticket.getLocation());
        
        if(by.equals("Date")) {
        	 statement.setString(5, newValue);
        }
        else statement.setString(5, ticket.getDate());
        
        if(by.equals("Quantity"))
        	statement.setInt(6,Integer.parseInt(newValue));
        else statement.setInt(6,ticket.getQuantity());
        
        if(by.equals("Price")) {
        	statement.setInt(7, Integer.parseInt(newValue));
        }
        else statement.setInt(7, ticket.getPrice());
        statement.setInt(8, ticket.getId());
        statement.executeUpdate();
	}
	/**Metoda pentru inserare bilet*/
	public void insertTicket(Ticket ticket) throws SQLException {
		PreparedStatement statement=null;
		String insertStatementString = "INSERT INTO ticket (id,artist,city,location,date,quantity,price)"
					+ " VALUES (?,?,?,?,?,?,?)";
		statement=connection.prepareStatement(insertStatementString);
		statement.setInt(1, ticket.getId());
        statement.setString(2, ticket.getArtist());
        statement.setString(3, ticket.getCity());
        statement.setString(4, ticket.getLocation());
        statement.setString(5, ticket.getDate());
        statement.setInt(6,ticket.getQuantity());
        statement.setInt(7, ticket.getPrice());
		statement.executeUpdate();
		}
	/**Metoda pentru stergere bilet*/
	public void deleteTicket(Ticket obj) throws SQLException{
		PreparedStatement delete=null;
		String insertStatementString = "DELETE FROM ticket WHERE ID=?";	
		delete=connection.prepareStatement(insertStatementString);
		delete.setInt(1, obj.getId());
		delete.executeUpdate();
	}
}
