package service;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.stereotype.Service;

import connection.MyConnection;
import dao.TicketDAO;
import dao.UserDAO;
import model.Ticket;
import model.User;
/**Clasa tip service pentru a transmite date intre controllere*/
@Service
public class CommonService {
	private TicketDAO ticketDAO=new TicketDAO(); //Clasele pentru operatii pe baza de date
	private UserDAO userDAO=new UserDAO();//Clasele pentru operatii pe baza de date
	private MyConnection connection=new MyConnection();	
	
	private User user;
	private ArrayList<Ticket> tickets;
	private boolean succesfulBuy=false;
	
	public CommonService() throws SQLException {
		ticketDAO.setConnection(connection.getConnection()); //Setare conexiuni cu baza de date
		userDAO.setConnection(connection.getConnection());
	}
	//Gettere si settere pentru acces incapsulat la date
	public void setUser(User user) {
		this.user=user;
	}
	public User getUser() {
		return user;
	}
	public TicketDAO getTDao() {
		return ticketDAO;
	}
	public UserDAO getUDao() {
		return userDAO;
	}
	public void setTicketList(ArrayList<Ticket> tickets) {
		this.tickets=tickets;
	}
	
	public ArrayList<Ticket> getTicketList(){
		return tickets;
	}
	
	public boolean getBuyStatus() {
		return succesfulBuy;
	}
	public void resetBuyStatus() {
		succesfulBuy=false;
	}
	
	public void confirmBuy() {
		succesfulBuy=true;
	}
}
