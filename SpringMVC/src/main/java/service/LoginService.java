package service;

import java.sql.SQLException;

import org.springframework.stereotype.Component;

import dao.UserDAO;
import model.User;
/**Clasa de service folosita pentru validare login*/
@Component
public class LoginService {
	private UserDAO userDAO;
	/**Validare parola*/
	public boolean validateUser(String userPassword,String password) throws SQLException {
		return password.equals(userPassword);
	}
	
	public void setUserDAO(UserDAO userDAO) {
		this.userDAO=userDAO;
	}
}
