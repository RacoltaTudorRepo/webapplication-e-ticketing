package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.stream.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import model.Ticket;
import model.User;

/** Clasa folosita pentru a printa biletul cerut de utilizator*/
public class TicketPrinter {
	private int index=0;
	
	void printTicket(Ticket ticket,User user,int quantity) throws FileNotFoundException, DocumentException {
		Document document=new Document();
		PdfWriter.getInstance(document, new FileOutputStream("OrderedTicket"+index+".pdf")); //Creare document
		
		document.open();
		
		
		
		Paragraph paragraph1 = new Paragraph("Details regarding the ordered ticket and person who ordered it: ");
		paragraph1.setSpacingBefore(30f);
		paragraph1.setSpacingAfter(30f);
		document.add(paragraph1);
		
		
		document.add(addClientTable(user));
		document.add(addTicketTable(ticket));
		
		Paragraph paragraph2 = new Paragraph("Total cost: \n"+ticket.getPrice()+"x"+quantity+"= "+ticket.getPrice()*quantity);
		paragraph2.setIndentationLeft(300f);
		document.add(paragraph2);
		document.close();
		index+=1;
	}
	
	public Chunk addText() {
		Font font = FontFactory.getFont(FontFactory.COURIER, 12, BaseColor.BLACK);
		Chunk chunk = new Chunk("Details regarding the ordered ticket and person who ordered it: ", font);
		return chunk;
	}
	/** Metoda pentru adaugare tabel client la document*/
	public PdfPTable addClientTable(User user) {
		PdfPTable table = new PdfPTable(3);
		table.setSpacingBefore(5f);
		table.setSpacingAfter(10f);
		addTableHeaderClient(table);
		addRowsClient(table,user);
		return table;
	}
	/** Metoda pentru creare header date utilizator*/
	private void addTableHeaderClient(PdfPTable table) {
	    Stream.of("Nume", "Prenume", "Adresa")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	/**Metoda pentru adaugare date client*/
	private void addRowsClient(PdfPTable table,User user) {
	    table.addCell(user.getNume());
	    table.addCell(user.getPrenume());
	    table.addCell(user.getAdresa());
	}
	/** Metoda pentru adaugare tabel ticket la document*/
	public PdfPTable addTicketTable(Ticket ticket) {
		PdfPTable table = new PdfPTable(6);
		table.setSpacingBefore(5f);
		table.setSpacingAfter(10f);
		addTableHeaderTicket(table);
		addRowsTicket(table,ticket);
		return table;
	}
	/** Metoda pentru creare header date bilet*/
	private void addTableHeaderTicket(PdfPTable table) {
	    Stream.of("ID", "Artist", "City","Location","Date","Price")
	      .forEach(columnTitle -> {
	        PdfPCell header = new PdfPCell();
	        header.setBackgroundColor(BaseColor.LIGHT_GRAY);
	        header.setBorderWidth(2);
	        header.setPhrase(new Phrase(columnTitle));
	        table.addCell(header);
	    });
	}
	/**Metoda pentru adaugare date bilet*/
	private void addRowsTicket(PdfPTable table,Ticket ticket) {
	    table.addCell(ticket.getId()+"");
	    table.addCell(ticket.getArtist());
	    table.addCell(ticket.getCity());
	    table.addCell(ticket.getLocation());
	    table.addCell(ticket.getDate());
	    table.addCell(ticket.getPrice()+"");
	}
}
