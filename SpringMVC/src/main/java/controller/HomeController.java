package controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.User;
import service.CommonService;
/**Clasa controller pentru pagina customer homepage*/
@Controller
public class HomeController {
	
	@Autowired
	private CommonService cservice; //Service-ul unde tinem date necesare
	/**Metoda pentru incarcarea paginii*/
	@RequestMapping(value = "/home", method = RequestMethod.GET)
	public String showSearch(ModelMap model) {
		User user=cservice.getUser(); //Asociere user
		model.put("nume",user.getNume());//Completarea tabelului de pe pagina cu datele personale
		model.put("prenume",user.getPrenume());
		model.put("adresa",user.getAdresa());
		model.put("role", user.getRole());
		if(cservice.getBuyStatus()==true) { //Daca s-a efectuat o cumparare, afisam mesajul aferent
			model.put("message","Your purchase was succesful! Enjoy the concert !");
			cservice.resetBuyStatus();//Resetam flag-ul
		}
		return "home";//Incarcare pagina home
	}
	/**Metoda pentru procesare date de pe pagina*/
	@RequestMapping(value = "/home", method = RequestMethod.POST)
	public String handleSearch(@RequestParam String searchFor,@RequestParam("value") String val,@RequestParam("button") String buton, ModelMap model) throws SQLException{
		System.out.println(buton);
		if(buton.equals("search")) {//Confirmare search
		if(val.equals("All concerts")) //Selectare bilete dupa filtrul confirmat
			cservice.setTicketList(cservice.getTDao().returnAllTickets());
		else if(val.equals("artist"))
			cservice.setTicketList(cservice.getTDao().returnTicketsByArtist(searchFor));
		else if(val.equals("city"))
			cservice.setTicketList(cservice.getTDao().returnTicketsByCity(searchFor));
		else if(val.equals("date"))
			cservice.setTicketList(cservice.getTDao().returnTicketsByDate(searchFor));
		return "redirect:ticketResult";}
		
		else return "redirect:login";//Confirmare logout
	}
}
