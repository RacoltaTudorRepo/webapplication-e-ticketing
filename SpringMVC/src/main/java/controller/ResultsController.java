package controller;

import java.io.FileNotFoundException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.text.DocumentException;

import model.Ticket;
import model.User;
import service.CommonService;

@Controller
/**Clasa controller pentru pagina de rezultate*/
public class ResultsController {
	
	@Autowired
	private CommonService cservice;
	private boolean fail=false;
	private User userHere;
	private boolean first=true;
	private TicketPrinter ticketPrinter=new TicketPrinter();
	
	/**Metoda pentru completarea datelor despre bilete pe pagina web cu cele cerute*/
	@RequestMapping(value = "/ticketResult", method = RequestMethod.GET)
	public String showSearch(ModelMap model) {
		ArrayList<Ticket> tickets=cservice.getTicketList(); //Preluam din service lista de bilete
		model.addAttribute("ticketList",tickets); //Trimitere la pagina web
		if(first==true) {
			userHere=cservice.getUser();//User folosit pentru printare bilet cu datele acestuia
			first=false;}
		if(fail==true)
			model.put("message","There are not enough tickets!");//Mesaj de eroare
		return "ticketResult";
		
	}
	/**Metoda folosita pentru confirmarea cumpararii unui bilet*/
	@RequestMapping(value = "/ticketResult", method = RequestMethod.POST)
	public String processBuy(@RequestParam("value") int ticketID, @RequestParam("quantity") int quantity, ModelMap model) throws SQLException {
		Ticket orderedTicket= cservice.getTDao().returnTicketByID(ticketID); //Asociere ticket cu id-ul cautat
		System.out.println(ticketID);
		if(orderedTicket.getQuantity()<quantity) {//Daca cantitatea ceruta e prea mare, redirectionare la cautare + afisare eroare
			fail=true;
			return "redirect:ticketResult";
		}
		cservice.getTDao().updateTicket(orderedTicket, quantity, "-"); //Dupa confirmare, update cantitate
		cservice.confirmBuy();
		//printare bilet
		
		System.out.println(userHere.getNume());
		
		try {
			ticketPrinter.printTicket(orderedTicket,userHere,quantity); //Printare bilet
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return "redirect:home";
	}
}
