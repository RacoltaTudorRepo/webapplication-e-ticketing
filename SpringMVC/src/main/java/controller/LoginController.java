package controller;

import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.User;
import service.CommonService;
import service.LoginService;


/**Clasa controller pentru pagina de login*/
@Controller //HANDLER - in functie de indentificare URL, va incarca pagina si o va trimite la servlet
public class LoginController{
	
	@Autowired
	private LoginService service; //Service folosit pentru validare login
	
	@Autowired
	private CommonService cservice;//Service folosit pentru a tine date despre user-ul logat
	
	
	@RequestMapping(value = "/login",method = RequestMethod.GET) //cand citeste url-ul asta + cel din WebXML
	//@ResponseBody //va scrie in body-ul site-ului
	//fara el, va incarca pagina .jsp returnata ca string
	public String showLoginPage() {
		return "login";
	}
	
	/**Metoda folosita pentru validarea login-ului*/
	@RequestMapping(value = "/login",method = RequestMethod.POST, params= {"name","password","radioName","buton"})
	public String handleLoginRequest(@RequestParam String name,@RequestParam String password,@RequestParam("radioName") String type,@RequestParam("buton") String buton,ModelMap model) throws SQLException {
	//System.out.println(buton);
		if(buton.equals("confirm signup")) //Daca efectuam signup, pagina de login va fi reincarcata
			return "redirect:login";
		
		User user= cservice.getUDao().findUserByUserName(name); //Asociere user cu cel logat din baza de date
		//System.out.println(user.getRole());
		//System.out.println("Radio"+type);
		if(user==null)
			{model.put("errorMessage", "Invalid user!");
			return "login";}
		if(service.validateUser(user.getPassword(),password)==false || type.equals(user.getRole())==false) //Validam parola
			{model.put("errorMessage", "Invalid user!");
			return "login";} //Parola incorecta->redirectionare pagina de login + eroare
		cservice.setUser(user); //Login cu succes, transmitere user la service
		if (user.getRole().equals("customer")) //Redirectionare care homepage customer/admin, dupa rolul user-ului
			return "redirect:home";
		else return "redirect:adminHome";
	}
	
	/**Metoda folosita pentru confirmare signup*/
	@RequestMapping(value = "/login",method = RequestMethod.POST, params= {"username","upass","uName","adress","buton","prenume"})
	public String handleSgnUp(@RequestParam("buton") String buton,@RequestParam String username,@RequestParam String upass,@RequestParam String uName,@RequestParam String adress,@RequestParam String prenume,ModelMap model) throws SQLException {
		//System.out.println(uName);
		cservice.getUDao().insertObject(new User(username,uName,prenume,upass,adress,"customer"));//Inserare user nou creat in baza de date
		model.put("errorMessage", "Signup was succesful!!");
		return "login";
}

	
}
