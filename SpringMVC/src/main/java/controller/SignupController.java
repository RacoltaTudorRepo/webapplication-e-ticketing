package controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
/**Clasa controller pentru pagina de signup*/
public class SignupController {
	/**Metoda pentru incarcarea paginii de signup*/
	@RequestMapping(value = "/index", method = RequestMethod.GET)
	public String showSignup(ModelMap model) {
		return "index";
	}
}
