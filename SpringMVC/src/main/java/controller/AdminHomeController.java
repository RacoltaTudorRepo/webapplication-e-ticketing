package controller;

import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import model.EditedTicket;
import model.ObjObserver;
import model.Ticket;
import model.User;
import service.CommonService;

/**Clasa controller pentru pagina de admin homepage*/
@Controller
public class AdminHomeController {
	@Autowired
	private CommonService cservice;


	private ObjObserver obs=new ObjObserver();
	
	/**Metoda pentru incarcarea paginii de admin*/
	@RequestMapping(value = "/adminHome", method = RequestMethod.GET)
	public String showSearch(ModelMap model) throws SQLException {
		User user=cservice.getUser(); //Preluarea user din service
		model.put("nume",user.getNume()); //Completare pagina web cu datele user-ului
		model.put("prenume",user.getPrenume());
		model.put("adresa",user.getAdresa());
		model.put("role", user.getRole());
		
		ArrayList<Ticket> tickets=cservice.getTDao().returnAllTickets(); //Preluarea tuturor biletelor din baza de date
		model.addAttribute("ticketList",tickets); //Incarcare date bilete pe pagina web
		return "adminHome";
	}
	/**Metoda pentru confirmare actiuni pe pagina*/
	@RequestMapping(value = "/adminHome", method = RequestMethod.POST)
	public String processModification(@RequestParam("value") int ticketID, @RequestParam("newValue") String newValue,@RequestParam("by") String by, ModelMap model,
			@RequestParam("newID") String newID,@RequestParam("newArtist") String newArtist, @RequestParam("newCity") String newCity,
			@RequestParam("newLocation") String newLocation,@RequestParam("newDate") String newDate,
			@RequestParam("newQuantity") String newQuantity, @RequestParam("newPrice") String newPrice,
			@RequestParam("radioVal") String radioVal,@RequestParam("button") String buton) throws SQLException { //Parametrii folositi pentru input
		
		if(buton.equals("submit")) { //Confirmare
			if(radioVal.equals("update")) {
			Ticket orderedTicket= cservice.getTDao().returnTicketByID(ticketID); //Asociere bilet cautat
			cservice.getTDao().updateTicketByAdmin(orderedTicket, newValue, by);
			orderedTicket=cservice.getTDao().returnTicketByID(ticketID);
			EditedTicket ticket=new EditedTicket(orderedTicket.getId(),orderedTicket.getArtist(),orderedTicket.getCity(),orderedTicket.getLocation(),orderedTicket.getDate(),orderedTicket.getQuantity(),orderedTicket.getPrice());
			ticket.addObserver(obs);
			ticket.setAction("update");
			} //Confirmare update in baza de date
			else if(radioVal.equals("delete")) {
				Ticket orderedTicket= cservice.getTDao().returnTicketByID(ticketID); //Asociere bilet

				cservice.getTDao().deleteTicket(orderedTicket);//Stergere bilet din baza de date
				
				EditedTicket ticket=new EditedTicket(orderedTicket.getId(),orderedTicket.getArtist(),orderedTicket.getCity(),orderedTicket.getLocation(),orderedTicket.getDate(),orderedTicket.getQuantity(),orderedTicket.getPrice());
				ticket.addObserver(obs);
				ticket.setAction("delete");
			}
			else {cservice.getTDao().insertTicket(new Ticket(Integer.parseInt(newID),newArtist,newCity,newLocation,newDate,Integer.parseInt(newQuantity),Integer.parseInt(newPrice)));
			//Inserare bilet nou
			EditedTicket ticket=new EditedTicket(Integer.parseInt(newID),newArtist,newCity,newLocation,newDate,Integer.parseInt(newQuantity),Integer.parseInt(newPrice));
			ticket.addObserver(obs);
			ticket.setAction("insert");}
			return "redirect:adminHome";} //Reincarcare pagina
		else return "redirect:login";//Logout
	}
}
