package connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
/**Clasa care ne va oferi conexiunea cu baza de date*/
public class MyConnection {
	private static final String DRIVER="com.mysql.cj.jdbc.Driver";
	private static final String DBURL="jdbc:mysql://localhost:3306/mydb";
	private static final String USER="root";
	private static final String PASS="12345";
	/**Instantiere driver*/
	public MyConnection() {
		try {
			Class.forName(DRIVER);
		}catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	/**Activarea efectiva a conexiunii*/
	public Connection getConnection() throws SQLException
	{	Connection connection;
		connection=DriverManager.getConnection(DBURL,USER,PASS);
		if(connection!=null) System.out.println("Successfull");
		return connection;
	}
}