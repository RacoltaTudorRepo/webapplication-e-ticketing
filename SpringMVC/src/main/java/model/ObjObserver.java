package model;

import java.util.Observable;
import java.util.Observer;

import org.springframework.stereotype.Component;


public class ObjObserver implements Observer{

	@Override
	public void update(Observable arg0, Object arg1) {
		EditedTicket ticket=(EditedTicket)arg0;
		if (ticket.getAction().equals("delete"))
			System.out.println("S-a sters biletul cu id-ul: "+ticket.getId());
		else if (ticket.getAction().equals("update"))
			System.out.println("Noile date ale biletului cu id-ul "+ticket.getId()+" : "+ticket.toString());
			else if (ticket.getAction().equals("insert"))
					System.out.println("A fost introdus biletul cu datele: "+ticket.toString());
		
	}

}
