package model;
/**Clasa pentru bilet*/
public class Ticket{
	private int id;
	private String artist;
	private String city;
	private String location;
	private int quantity;
	private String date;
	private int price;
	public Ticket(int id,String artist, String city, String location,String date, int quantity,int price) {
		this.artist=artist;
		this.city=city;
		this.location=location;
		this.quantity=quantity;
		this.id=id;
		this.date=date;
		this.price=price;
	}
	public int getId() {
		return id;
	}
	public String getArtist() {
		return artist;
	}
	public String getCity() {
		return city;
	}
	public String getLocation() {
		return location;
	}
	public int getQuantity() {
		return quantity;
	}
	public String getDate() {
		return date;
	}
	public int getPrice() {
		return price;
	}
}
