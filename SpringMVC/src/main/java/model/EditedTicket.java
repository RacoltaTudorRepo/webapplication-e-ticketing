package model;

import java.util.Observable;

public class EditedTicket extends Observable{
	private int id;
	private String artist;
	private String city;
	private String location;
	private int quantity;
	private String date;
	private int price;
	private String action;
	public EditedTicket(int id,String artist, String city, String location,String date, int quantity,int price) {
		this.artist=artist;
		this.city=city;
		this.location=location;
		this.quantity=quantity;
		this.id=id;
		this.date=date;
		this.price=price;
	
	}
	
	public void setAction(String action) {
		this.action=action;
		setChanged();
		notifyObservers();
	}
	
	public String toString() {
		String data=new String();
		data="Id-ul biletului: "+this.id+
				"\n\t Artist: "+this.artist+
				"\n\t Oras: "+this.city+
				"\n\t Locatie: "+this.location+
				"\n\t Cantitate: "+this.quantity+
				"\n\t Data: "+this.date+
				"\n\t Pret: "+this.price;
		return data;
	}
	
	public int getId() {
		return id;
	}
	public String getArtist() {
		return artist;
	}
	public String getCity() {
		return city;
	}
	public String getLocation() {
		return location;
	}
	public int getQuantity() {
		return quantity;
	}
	public String getDate() {
		return date;
	}
	public int getPrice() {
		return price;
	}
	public String getAction() {
		return action;
	}
}
