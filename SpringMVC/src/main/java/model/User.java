package model;
/**Clasa pentru user*/
public class User {
	private String userName;
	private String nume;
	private String prenume;
	private String password;
	private String adresa;
	private String role;
	public User(String userName,String nume,String prenume,String password,String adresa,String role) {
		this.setNume(nume);
		this.setAdresa(adresa);
		this.setPrenume(prenume);
		this.setRole(role);
		this.setPassword(password);
		this.setUserName(userName);
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}
	public String getNume() {
		return nume;
	}
	public void setNume(String nume) {
		this.nume = nume;
	}
	public String getPrenume() {
		return prenume;
	}
	public void setPrenume(String prenume) {
		this.prenume = prenume;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
}
