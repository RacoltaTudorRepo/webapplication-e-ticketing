package test;

import java.sql.SQLException;
import java.util.ArrayList;

import connection.MyConnection;
import dao.TicketDAO;
import dao.UserDAO;
import model.Ticket;
import model.User;

public class Test {
	public static void main(String[] args) throws SQLException {
		MyConnection connection=new MyConnection();
		UserDAO user=new UserDAO();
		user.setConnection(connection.getConnection());
		User useri=user.findUserByUserName("racoltatudor");
		System.out.println(useri.getNume()+useri.getPrenume()+useri.getRole());
		
		
		TicketDAO tic=new TicketDAO();
		tic.setConnection(connection.getConnection());
		ArrayList<Ticket> tickets=new ArrayList<Ticket>();
		tickets=tic.returnTicketsByArtist("Marshmello");
		for(Ticket t:tickets) {
			System.out.println(t.getId()+t.getArtist()+t.getCity()+t.getQuantity());
		}
	}
}
